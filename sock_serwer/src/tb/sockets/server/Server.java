package tb.sockets.server;

import java.io.*;
import java.net.*;

import tb.sockets.server.MainFrame;

public class Server 
{
	public static int pozycja = 10, received=10;
	public int popPoz = 10, popRec = 10;
	static boolean tura = false, cont = true;
	static ServerSocket sersock;
	Socket sock;
	BufferedReader keyRead;
	OutputStream ostream;
	PrintWriter pwrite;
	InputStream istream;
	BufferedReader receiveRead;
	public Server() throws Exception
	{
		sersock = new ServerSocket(3000);
	    sock = sersock.accept();
	    ostream = sock.getOutputStream(); 
	    pwrite = new PrintWriter(ostream, true);
	    istream = sock.getInputStream();
	    receiveRead = new BufferedReader(new InputStreamReader(istream));
		int moves =0;
		if(sock.isConnected())
		{
				while(cont == true)
				{
					pwrite.flush();
					if(tura == true)					//wysylanie
					{
						if(pozycja!= popPoz)			//zabezpieczenie przed powtorkami
						{
							pwrite.write(pozycja);
							moves++;
							popPoz = pozycja;
							tura = false;
							pwrite.flush();
							if(moves>0)					//sprawdzanie wyniku
								MainFrame.checkForWin();
						}
					}
					if(tura == false)					//odbieranie
					{
						
						received = receiveRead.read();
						if(received != popRec)			//zabezpieczenie przed powtorkami
						{
							MainFrame.getOpponentsMove(received);	//interpretacja ruchu
							moves++;
							popRec = received;
							tura = true;
							MainFrame.turn.setText("Twoj ruch");
							if(moves>0)					//sprawdzanie wyniku
								MainFrame.checkForWin();
						}
					}
					if(sock.isClosed())
					{
						cont=false;
					}
				}
			}
				sock.close();
				ostream.close();
				pwrite.close();
				istream.close();
				receiveRead.close();
	}
}
