package tb.sockets.server;

import java.awt.EventQueue;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import tb.sockets.server.Server;
import tb.sockets.server.kontrolki.KKButton;
import tb.sockets.server.OrderPane;

import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.border.LineBorder;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	public static int [][] zajete = new  int[3][3];
	static int wins = 0, opnt = 0;
	public static OrderPane panel;
	public static JButton btn1_1 , btn1_2 , btn1_3 , btn2_1 , btn2_2 , btn2_3 , btn3_1 , btn3_2 , btn3_3;
	public static JLabel roundCount, score, turn, endOfRndLbl, countdown, resLbl;
	static int i = 1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		new Thread() {
			public void run() {
				try {
					Server server = new Server();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public MainFrame() throws IOException {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((screen.width-622)/2, (screen.height-520)/2, 622, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Serwer");
		this.setResizable(false);
		
		panel = new OrderPane();										//plansza
		panel.setBounds(145, 14, 450, 450);
		contentPane.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));	//panel z przyciskami
		panel_1.setBounds(10, 177, 123, 123);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(3, 3, 5, 5));
		
		roundCount = new JLabel("RUNDA 1", SwingConstants.CENTER);		//licznik rund
		contentPane.add(roundCount);
		roundCount.setBounds(10, 20, 130, 23);
		
		score = new JLabel("0:0", SwingConstants.CENTER);				//wynik
		contentPane.add(score);
		score.setBounds(10,50,130,23);
		
		turn = new JLabel("Ruch przeciwnika", SwingConstants.CENTER);			//wskaznik tury
		contentPane.add(turn);
		turn.setBounds(10,80,130,23);
		
		resLbl = new JLabel("", SwingConstants.CENTER);					//wynik rundy
		contentPane.add(resLbl);
		resLbl.setBounds(10, 320, 130, 23);
		
		endOfRndLbl = new JLabel("", SwingConstants.CENTER);			//wskaznik konca rundy
		contentPane.add(endOfRndLbl);
		endOfRndLbl.setBounds(10, 345, 130, 23);
		
		countdown = new JLabel("", SwingConstants.CENTER);				//odliczanie
		contentPane.add(countdown);
		countdown.setBounds(10,370, 130, 23);
		
		btn1_1 = new KKButton(true);									//przyciski
		btn1_1.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 0);
					Server.pozycja = 0;
					zajete[0][0] = 1;
					btn1_1.setEnabled(false);
				}
			}
		});
		panel_1.add(btn1_1);
		
		btn1_2 = new KKButton(true);
		btn1_2.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 1);
					Server.pozycja = 1;
					zajete[0][1] = 1;
					btn1_2.setEnabled(false);
				}
			}
		});
		panel_1.add(btn1_2);

		btn1_3 = new KKButton(true);
		btn1_3.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 2);	
					Server.pozycja = 2;
					zajete[0][2] = 1;
					btn1_3.setEnabled(false);
				}
			}
		});
		panel_1.add(btn1_3);

		btn2_1 = new KKButton(true);
		btn2_1.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 3);
					Server.pozycja = 3;
					zajete[1][0] = 1;
					btn2_1.setEnabled(false);
				}
			}
		});
		panel_1.add(btn2_1);
		
		btn2_2 = new KKButton(true);
		btn2_2.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 4);
					Server.pozycja = 4;
					zajete[1][1] = 1;
					btn2_2.setEnabled(false);
				}
			}
		});
		panel_1.add(btn2_2);
		
		btn2_3 = new KKButton(true);
		btn2_3.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 5);	
					Server.pozycja = 5;
					zajete[1][2] = 1;
					btn2_3.setEnabled(false);
				}
			}
		});
		panel_1.add(btn2_3);
		
		btn3_1 = new KKButton(true);
		btn3_1.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 6);	
					Server.pozycja = 6;
					zajete[2][0] = 1;
					btn3_1.setEnabled(false);
				}
			}
		});
		panel_1.add(btn3_1);
		
		btn3_2 = new KKButton(true);
		btn3_2.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 7);	
					Server.pozycja = 7;
					zajete[2][1] = 1;
					btn3_2.setEnabled(false);
				}
			}
			
		});
		panel_1.add(btn3_2);
		
		btn3_3 = new KKButton(true);
		btn3_3.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					turn.setText("Ruch przeciwnika");
					panel.drawShape(true, 8);	
					Server.pozycja = 8;
					zajete[2][2] = 1;
					btn3_3.setEnabled(false);
				}
			}
		});
		panel_1.add(btn3_3);
	}

/*
 * 
 * interpretacja ruchu przeciwnika
 * 	
 */
	
	
public static void getOpponentsMove(int wybor)
	{
		switch(wybor/3)
		{
		case 0:
		{
			switch(wybor%3)
			{
			case 0: panel.drawShape(false, 0); btn1_1.setEnabled(false); zajete[0][0] = 2; break;
			case 1: panel.drawShape(false, 1); btn1_2.setEnabled(false); zajete[0][1] = 2; break;
			case 2: panel.drawShape(false, 2); btn1_3.setEnabled(false); zajete[0][2] = 2; break;
			}
			break;
		}
		case 1:
		{
			switch(wybor%3)
			{
			case 0: panel.drawShape(false, 3); btn2_1.setEnabled(false); zajete[1][0] = 2; break;
			case 1: panel.drawShape(false, 4); btn2_2.setEnabled(false); zajete[1][1] = 2; break;
			case 2: panel.drawShape(false, 5); btn2_3.setEnabled(false); zajete[1][2] = 2; break;
			}
			break;
		}
		case 2:
		{
			switch(wybor%3)
			{
			case 0: panel.drawShape(false, 6); btn3_1.setEnabled(false); zajete[2][0] = 2; break;
			case 1: panel.drawShape(false, 7); btn3_2.setEnabled(false); zajete[2][1] = 2; break;
			case 2: panel.drawShape(false, 8); btn3_3.setEnabled(false); zajete[2][2] = 2; break;
			}
			break;
		}
	}
}


/*
 * 
 *	sprawdzanie wyniku i ewentualny reset planszy
 * 
 */


public static void checkForWin()
	{
		int wl = 0;
		Graphics2D g2w = (Graphics2D) panel.getGraphics();
		g2w.setStroke(new BasicStroke(5));
		g2w.setColor(Color.GREEN);
		if(zajete[0][0] == zajete [0][1] && zajete[0][1] == zajete[0][2] && zajete[0][0]!=0 && zajete[0][1]!=0 && zajete[0][2]!=0)		//8 warunkow na wygrana, 1 na remis
		{
			g2w.drawLine(20,75,430,75);
			if(zajete[0][0] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
		if(zajete[0][0] == zajete [1][0] && zajete[1][0] == zajete[2][0] && zajete[0][0]!=0 && zajete[1][0]!=0 && zajete[2][0]!=0)
		{
			g2w.drawLine(75,20,75,430);
			if(zajete[0][0] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
		if(zajete[0][0] == zajete [1][1] && zajete[1][1] == zajete[2][2] && zajete[0][0]!=0 && zajete[1][1]!=0 && zajete[2][2]!=0)
		{
			g2w.drawLine(20,20,430,430);
			if(zajete[0][0] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
		if(zajete[1][0] == zajete [1][1] && zajete[1][1] == zajete[1][2] && zajete[1][0]!=0 && zajete[1][1]!=0 && zajete[1][2]!=0)
		{
			g2w.drawLine(20,225,430,225);
			if(zajete[1][0] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}	
		}
		if(zajete[2][0] == zajete [2][1] && zajete[2][1] == zajete[2][2] && zajete[2][0]!=0 && zajete[2][1]!=0 && zajete[2][2]!=0)
		{
			g2w.setStroke(new BasicStroke(5));
			g2w.setColor(Color.GREEN);
			g2w.drawLine(20,375,430,375);
			if(zajete[2][0] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
		if(zajete[0][1] == zajete [1][1] && zajete[1][1] == zajete[2][1] && zajete[0][1]!=0 && zajete[1][1]!=0 && zajete[2][1]!=0)
		{
			g2w.drawLine(225,20,225,430);
			if(zajete[0][1] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
	if(zajete[0][2] == zajete [1][2] && zajete[1][2] == zajete[2][2] && zajete[0][2]!=0 && zajete[1][2]!=0 && zajete[2][2]!=0)
		{
			g2w.drawLine(375,20,375,430);
			if(zajete[0][2] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
		if(zajete[0][2] == zajete [1][1] && zajete[1][1] == zajete[2][0] && zajete[0][2]!=0 && zajete[1][1]!=0 && zajete[2][0]!=0)
		{
			g2w.drawLine(20,430,430,20);
			if(zajete[0][2] == 1)
			{
				wins++;
				wl = 2;
			}
			else 
			{
				opnt++;
				wl = 1;
			}
		}
		if(zajete[0][0]!=0 && zajete[0][1]!=0 && zajete[0][2]!=0 && zajete[1][0]!=0 && zajete[1][1]!=0 && zajete[1][2]!=0 && zajete[2][0]!=0 && zajete[2][1]!=0 &&zajete[2][2]!=0 && wl==0)
		{
			wl = 3;
		}
		if(wl > 0)
		{
			switch(wl)									//wynik rundy
			{
				case 1: resLbl.setText("Przegrana"); break;
				case 2: resLbl.setText("Wygrana"); break;
				case 3: resLbl.setText("Remis"); break;
			}
			score.setText(wins + ":" + opnt);			//wynik gry
			for(int i=5;i>0;i--)						
			{
			endOfRndLbl.setText("Koniec rundy");
			countdown.setText(Integer.toString(i));
			endOfRndLbl.setVisible(true);
			countdown.setVisible(true);
			try {										//odliczanie
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			}
			endOfRndLbl.setVisible(false);
			countdown.setVisible(false);
			resLbl.setVisible(false);
			for(int k = 0 ; k<3 ; k++)					//zerowanie tablicy, nowa plansza, resetowanie przyciskow
			{
				for(int j = 0; j<3 ; j++)
				{
					zajete[k][j] = 0;
				}
			}
			panel.paintComponent(panel.getGraphics());
			btn1_1.setEnabled(true);
			btn1_2.setEnabled(true);
			btn1_3.setEnabled(true);
			btn2_1.setEnabled(true);
			btn2_2.setEnabled(true);
			btn2_3.setEnabled(true);
			btn3_1.setEnabled(true);
			btn3_2.setEnabled(true);
			btn3_3.setEnabled(true);
			roundCount.setText("RUNDA " + ++i);
		}
	}
}