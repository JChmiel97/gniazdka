package tb.sockets.client;

import javax.swing.JPanel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;


public class OrderPane extends JPanel {
	
	/**
	 * Create the panel.
	 */
	public OrderPane() {
		super();
		setBackground(new Color(255, 255, 240));
		
	}
	
	public void drawShape(boolean wybor, int okno)
	{
		Graphics2D g2 = (Graphics2D) getGraphics();
		g2.setStroke(new BasicStroke(5));
		if(wybor == true)
		{
			g2.setColor(Color.BLUE);
			g2.drawOval(10+((okno%3)*150), 10+((okno/3)*150), 130, 130);
		}
		if(wybor == false)
		{
			g2.setColor(Color.red);
			g2.drawLine(20+((okno%3)*150), 20+((okno/3)*150), 130+((okno%3)*150), 130+((okno/3)*150));
			g2.drawLine(20+((okno%3)*150), 130+((okno/3)*150), 130+((okno%3)*150), 20+((okno/3)*150));
		}
	}
	public void clearGrid()
	{
		int i,j;
		Graphics2D g3 = (Graphics2D) getGraphics();
		for(i = 0 ; i<3 ; i++)
		{
			for(j = 0 ; j<3 ; j++)
			g3.setColor(Color.white);
			g3.fillRect(i*150+1, j*150+1, 148, 148);
		}
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 450, 450);
		g.setColor(Color.BLACK);
		g.drawLine(150, 0, 150, 450);
		g.drawLine(300, 0, 300, 450);
		g.drawLine(0, 150, 450, 150);
		g.drawLine(0, 300, 450, 300);
		g.drawRect(0, 0, 449, 449);
	}
}