/**
 * 
 */
package tb.sockets.client.kontrolki;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

/**
 * @author tb
 *
 */
@SuppressWarnings("serial")
public class KKButton extends JButton {

	private BufferedImage[] rysunki = new BufferedImage[9];
	private int stan = 0;
	
	public KKButton(boolean wybor) {
		super("");
		for (int i=0;i<9; i++) {
			BufferedImage tI = new BufferedImage(40, 40, BufferedImage.TYPE_INT_RGB);
			Graphics2D gI = (Graphics2D) tI.getGraphics();
			gI.setColor(Color.white);
			if(wybor == true)
			{
			gI.drawOval(3, 3, 27, 27);
			}
			else
			{
				gI.drawLine(6, 6 , 28, 28);
				gI.drawLine(6, 28, 28, 6);
			}
			rysunki[i] = tI;
		}
	}
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.drawImage(rysunki[stan], 0, 0, null);
	}
}
